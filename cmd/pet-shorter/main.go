package main

import (
	"fmt"
	"log"
	"shorter/internal/repo"
	"shorter/internal/server"
	"shorter/internal/service"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	mongo, err := repo.NewMongoDB()
	if err != nil {
		return fmt.Errorf("new mongo db: %s", err)
	}
	defer mongo.Close()

	memCache, err := repo.NewMemCache()
	if err != nil {
		return fmt.Errorf("new mem cache: %s", err)
	}

	shorterSrv, err := service.New(mongo, memCache)
	if err != nil {
		return fmt.Errorf("new service: %s", err)
	}
	defer shorterSrv.Close()

	httpServer, err := server.New(shorterSrv)
	if err != nil {
		return fmt.Errorf("new http server: %s", err)
	}
	defer httpServer.Close()

	if err := httpServer.Run(":8083"); err != nil {
		return fmt.Errorf("run server: %s", err)
	}

	//TODO: gracefully shutdown
	return nil
}
