package service

import (
	"context"
	"fmt"
	"shorter/internal/core"
	"shorter/internal/repo"
)

type Service struct {
	cache   repo.CacheRepo
	urlRepo repo.UrlRepo
}

func New(mongoDBRepo repo.UrlRepo, cache repo.CacheRepo) (*Service, error) {
	return &Service{
		cache:   cache,
		urlRepo: mongoDBRepo,
	}, nil
}

func (s *Service) Add(ctx context.Context, longUrl, shortPath string) (string, error) {
	if shortPath == "" {
		shortPath = core.RandStringBytes(longUrl)
	}
	err := s.urlRepo.Add(ctx, longUrl, shortPath)
	if err != nil {
		return "", fmt.Errorf("add data to url repo: %s", err)
	}
	s.cache.Add(ctx, longUrl, shortPath)

	return shortPath, nil
}

func (s *Service) Get(ctx context.Context, shortPath string) (string, error) {
	longUrl, exist := s.cache.Get(ctx, shortPath)
	if exist {
		return longUrl, nil
	}
	longUrl, err := s.urlRepo.Get(ctx, shortPath)
	if err != nil {
		return "", fmt.Errorf("get data from url repo")
	}

	return longUrl, nil
}

func (s *Service) Delete(ctx context.Context, shortPath string) error {
	err := s.urlRepo.Delete(ctx, shortPath)
	if err != nil {
		return fmt.Errorf("delete data from url repo")
	}
	return nil
}

func (s *Service) Close() error {
	return nil
}
