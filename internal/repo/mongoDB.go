package repo

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var NotFoundErr = errors.New("record not found")

type MongoDBRepo struct {
	client *mongo.Client
}

func NewMongoDB() (*MongoDBRepo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	clientOptions := &options.ClientOptions{
		Auth: &options.Credential{
			Username: "pet",
			Password: "shorter",
		},
	}
	client, err := mongo.Connect(ctx, clientOptions.ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		return nil, fmt.Errorf("new mongo client: %s", err)
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, fmt.Errorf("ping mongo: %s", err)
	}

	return &MongoDBRepo{
		client: client,
	}, nil
}

func (m *MongoDBRepo) Add(ctx context.Context, longUrl, shortPath string) error {
	collection := m.client.Database("testing").Collection("urls")

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	_, err := collection.InsertOne(ctx, bson.D{{"longUrl", longUrl}, {"shortPath", shortPath}})
	if err != nil {
		return fmt.Errorf("inser one: %s", err)
	}

	return nil
}

func (m *MongoDBRepo) Get(ctx context.Context, shortPath string) (string, error) {
	collection := m.client.Database("testing").Collection("urls")
	var result struct {
		LongUrl string
	}
	filter := bson.D{{"shortPath", shortPath}}
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return "", NotFoundErr
		}
		return "", fmt.Errorf("get record: %s", err)
	}
	return result.LongUrl, nil
}

func (m *MongoDBRepo) Delete(ctx context.Context, shortPath string) error {
	//TODO implement me
	panic("implement me")
}

func (m *MongoDBRepo) Close() error {
	return m.client.Disconnect(context.Background())
}
