package repo

import (
	"context"
	"sync"
	"time"
)

const (
	defaultTTL = time.Minute * 15
)

type MemCache struct {
	cache map[string]cacheValue
	mu    sync.Mutex
}

func NewMemCache() (*MemCache, error) {
	return &MemCache{
		cache: make(map[string]cacheValue),
		mu:    sync.Mutex{},
	}, nil
}

func (c *MemCache) Add(ctx context.Context, longUrl, shortPath string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.cache[shortPath] = cacheValue{
		LongUrl: longUrl,
		Added:   time.Now(),
	}
}
func (c *MemCache) Get(ctx context.Context, shortPath string) (string, bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	value, exist := c.cache[shortPath]
	return value.LongUrl, exist
}

func (c *MemCache) Delete(ctx context.Context, shortPath string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	delete(c.cache, shortPath)
}

func (c *MemCache) Clean(ctx context.Context) {
	for shortPath, data := range c.cache {
		if data.Added.Add(defaultTTL).Before(time.Now()) {
			c.Delete(ctx, shortPath)
		}
	}
}

type cacheValue struct {
	LongUrl string
	Added   time.Time
}
