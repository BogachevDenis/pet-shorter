package repo

import "context"

type UrlRepo interface {
	Add(ctx context.Context, longUrl, shortPath string) error
	Get(ctx context.Context, shortPath string) (string, error)
	Delete(ctx context.Context, shortPath string) error
}

type CacheRepo interface {
	Add(ctx context.Context, longUrl, shortPath string)
	Get(ctx context.Context, shortPath string) (string, bool)
	Delete(ctx context.Context, url string)
}
