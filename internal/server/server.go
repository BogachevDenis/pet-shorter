package server

import (
	"encoding/json"
	"net/http"
	"shorter/internal/service"

	"github.com/go-chi/chi/v5"
)

func New(service *service.Service) (*Server, error) {
	r := chi.NewRouter()
	server := &Server{
		service: service,
		router:  r,
	}
	r.Route("/api", func(r chi.Router) {
		r.HandleFunc("/{path}", server.Main)
		r.HandleFunc("/add", server.Add)
		r.HandleFunc("/delete", server.Delete)
	})

	return server, nil
}

type Server struct {
	service *service.Service
	router  *chi.Mux
}

func (s *Server) Main(w http.ResponseWriter, r *http.Request) {
	path := chi.URLParam(r, "path")
	longUrl, err := s.service.Get(r.Context(), path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	http.Redirect(w, r, longUrl, http.StatusSeeOther)
}

func (s *Server) Add(w http.ResponseWriter, r *http.Request) {
	req := struct {
		Url        string `json:"url"`
		CustomPath string `json:"custom_path"`
	}{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		http.Error(w, "decode req: "+err.Error(), http.StatusBadRequest)
		return
	}
	shortPath, err := s.service.Add(r.Context(), req.Url, req.CustomPath)
	if err != nil {
		http.Error(w, "add: "+err.Error(), http.StatusBadRequest)
		return
	}
	bytes, err := json.Marshal(shortPath)
	if err != nil {
		http.Error(w, "marshal short url: "+err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(bytes)
}

func (s *Server) Delete(w http.ResponseWriter, r *http.Request) {
	req := struct {
		Url string `json:"url"`
	}{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		http.Error(w, "decode req: "+err.Error(), http.StatusBadRequest)
		return
	}
	err = s.service.Delete(r.Context(), req.Url)
	if err != nil {
		http.Error(w, "delete url: "+err.Error(), http.StatusBadRequest)
		return
	}
}

func (s *Server) Run(addr string) error {
	return http.ListenAndServe(addr, s.router)
}

func (s *Server) Close() error {
	return nil
}
