FROM golang:1.18 AS builder

RUN mkdir /app

WORKDIR /app/

COPY . .

RUN go build cmd/pet-shorter/main.go

FROM debian:buster-slim

WORKDIR /root/

COPY --from=builder /app/main .



ENTRYPOINT ["./main"]