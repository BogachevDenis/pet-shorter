# pet-shorter

#### Сервис для сокращения ссылок как [тык](https://bitly.com/)

### Scheme:

![](docs/pet-shorter.png)

### REST API:
- POST /add  
> req - {"url":"string", "custom_path":"string"}   
> resp - {"path":"string"}   

- POST /delete  
> req - {"url":"string"}   

- GET /{path}  

### Metrics:
#### Technical metrics:
- RPS
- Response time
#### User metrics:
- Country
- IP

